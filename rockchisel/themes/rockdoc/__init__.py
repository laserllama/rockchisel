THEME_META = dict(
	author = "Michael DeHaan",
	email = "michael@michaeldehaan.net",
	license = "MIT",
	url = "http://rockchisel.com",
	version = "0.1"
)

VARIABLES = dict(
	foo = 'bar',
	baz = 123
)

SNIPPETS = [
	'footer', 'header', 'topleft'
]

MACROS = [
	'macros', 'user_macros'
]

