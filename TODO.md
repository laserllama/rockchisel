TODO
====

* snippets for "see also" blocks and other useful bootstrap features (?)
* add macro for "new_in(version)" 
* license headers
* rockdoc theme - add a google analytics setting, include if set
* add a 'themes' section to the official docs
* add more 'how to use jinja2' info in the official docs
* all kinds of good user ideas and pull requests!

