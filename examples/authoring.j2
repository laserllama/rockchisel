<h1>Authoring</h1>

Writing content in RockChisel is easy, because beyond a basic python definition file, RockChisel is basically HTML.

The very best source to understand RockChisel is reading through the source code to the {{ link("https://bitbucket.org/laserllama/rockchisel/src/master/examples/", "RockChisel documentation itself") }}.
First find and read 'site.py', and then browse the templates.

<!-------------------------------------------------------------------------------------------------------->

{{ table_of_contents() }}

<!-------------------------------------------------------------------------------------------------------->

{{ section("Builder Configuration") }}

First, let's look at an example configuration file, {{ link("https://bitbucket.org/laserllama/rockchisel/src/master/examples/site.py", "site.py") }}.  The name
isn't actually important, but what is interesting is that it is an executable file - this means that RockChisel does not have (or need) a command line tool.
Building the project is discussed later in {{ doc("building") }}.

{{ begin_code(language="python") }}#!/usr/bin/python
{% raw %}
from rockchisel import Builder
import os

path = os.path.dirname(os.path.realpath(__file__))

site = Builder(

	input_path  = path,
	output_path = os.path.join(path, "output"),
	theme = "rockchisel.themes.rockdoc",

	variables = dict(version = 0.1),
	page_title_template = "Rock Chisel {{ version }}: {{ title }}",

	sections = {
			"User Guide": {
				"Home" : "index",
				"Installation": "installation",
				"Authoring": "authoring",
				"Building": "building",
				"Uploading": "uploading",
				"Changelog": "changelog"
			},
			"Community": {
				"License": "license",
				"Contributing": "contributing",
				"Themes": "themes"
			}
	}

)

site.build(){% endraw %}{{ end_code() }}


Let's go through some of the settings step by step.

<ul>
<li><b>input_path</b> - this is where to find the templates that will make up your documentation. The code above just
finds it in the same directory as site.py.</li>
<li><b>output_path</b> - this is where the documentation project will be rendered. Warning: content here will be erased!</li>
<li><b>theme</b> - what theme to use - we ship with one theme called 'rockdoc'. This is discussed later in {{ doc("themes") }}</li>
<li><b>variables</b> - adds any template variables to inject into a template, like a version number or a date</li>
<li><b>page_title_template</b> - this is used to render the HTML title tag so the name of the project is in each title.</li>
<li><b>sections</b> - this defines the layout of the navigation sidebar. Any template rendered <i>must</i> be in this structure.</li>
</ul>

<!-------------------------------------------------------------------------------------------------------->

{{ section("Templates") }}

As you can see from the {{ link("https://bitbucket.org/laserllama/rockchisel/src/master/examples/", "RockChisel documentation") }}, the project
consists of a number of {{ link("https://jinja.palletsprojects.com/en/2.11.x/", "Jinja2") }} HTML templates.  When writing documentation
in RockChisel, you have the full power of HTML, and are not limited by obscure sub-dialects of Restructured Text or Markdown.

Variables marked in <b>site.py</b> can be used in templates, as can larger blocks of other files, known as snippets, which we will
explain later in this chapter.

The easiest way to understand the template system might be to see the source of {{ link("https://bitbucket.org/laserllama/rockchisel/src/master/examples/building.j2", "the previous chapter") }}.

<!-------------------------------------------------------------------------------------------------------->

{{ section("Macros") }}

Various HTML shortcuts are available, as described in {{ doc('macro') }}.  You do not need to use macros unless you want to, but they can add consistency to your
documentation.

<!-------------------------------------------------------------------------------------------------------->

{{ section("Snippets") }}

Portions of the the theme can be easily replaced without picking a new theme.  This feature is discussed in {{ doc("themes") }}.
For instance, the word "RockChisel" in the upper left corner of the documentation is a snippet called "topleft.j2".

These are used to make common page headers and footers, or to replace a site logo.  Many users will be able to use the stock theme, and we will continue
to improve the stock theme over time. If you want to, you may wish to copy the theme to make it your own.

The RockDoc theme has three snippets - "topleft.j2", "header.j2", and "footer.j2".  Header and footer are essentially plank, and can be used to, for instance,
insert copyright messages or tracking codes.

<!-------------------------------------------------------------------------------------------------------->

{{ section("Images and Other Static Files") }}

If your documentation directory wants to include any graphics or other static files, including custom javascript or CSS, just include them
in project directory at the top level, and they will be copied over to the output directory automatically. You can reference them with
regular HTML.

<!-------------------------------------------------------------------------------------------------------->

{{ section("File Structure") }}

Currently all Jinja2 templates should be placed at the top level of your input directory. This isn't extremely flexible,
but we suspect it won't matter for most purposes.  This could change later.

<!-------------------------------------------------------------------------------------------------------->

{{ section("Next") }}

With some basic understanding of the system, now it is time to {{ doc('building', 'build the docs') }}.  Don't forget to skim {{ doc('macro') }} if you haven't already.

