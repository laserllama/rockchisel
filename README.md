rockchisel
==========

rockchisel is a new documentation framework and static website generator

Status
======

BETA! - future updates will come frequently and remain fully compatible with existing projects

Website
=======

http://rockchisel.com - includes install instructions and examples

License
=======

MIT

Author
======

Michael DeHaan <michael AT michaeldehaan.net>

