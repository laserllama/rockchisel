html:
	(cd examples; make)

clean:
	-(rm dist/*)

wheel: clean
	python setup.py sdist bdist_wheel

pypi_upload: wheel
	# project owners only, after bumping version
	twine upload dist/*.tar.gz
