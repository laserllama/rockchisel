from rockchisel import Builder
import os

path = os.path.dirname(os.path.realpath(__file__))

VERSION = 0.7

site = Builder(
	
	input_path  = path,
	output_path = os.path.join(path, "output"),
	theme = "rockchisel.themes.rockdoc",

	variables = dict(version = VERSION),
	page_title_template = "Rock Chisel {{ version }}: {{ title }}",
	
	index = "index",
	sections = {
			"User Guide": {
				"Home" : "index",
				"Installation": "installation",
				"Authoring": "authoring",
				"Macros" : "macro",
				"Building": "building",
				"Changelog": "changelog"
			},
			"Community": {
				"License": "license",
				"Contributing": "contributing",
				"Themes": "themes"
			}
	},
	
	theme_options = dict(
		sidebar_background = "#FF0000"
	)

)

site.build()


